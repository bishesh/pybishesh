# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
Utility functions related to transformation matrices (poses).
"""

from .files import lines_as_list
import numpy as np

def read_pose(fname, fformat='opencv'):    
    """
    Read pose from input file
    Arguments
    ---------
    fname: str
        Input filename to read pose from
    fformat: str
        Pose format. Supports 'ifindviewer', 'opencv' and 'itk'
    Returns
    -------
    pose: numpy 4X4 array
        pose read from the file

"""
    if fformat=='opencv':
        with open(fname, 'r') as fil:
            lines = [line.strip() for line in fil if '.' in line]

        pose = ''.join(lines[1:]) # Ignore the header yaml        
        pose = pose[8:-2] # Remove data: [ in the beginning, and ] at the end
        pose = [x.strip() for x in pose.split(',')] # Extract it into a list
        
    elif fformat=='ifindviewer':
        trck = [x for x in lines_as_list(fname, '#') if 'TrackerMatrix' in x][0]
        trck = trck.partition('=')[2]
        pose = trck.strip('').split(' ')
    elif fformat=='itk':
        m = lines_as_list(fname, '#')
        pose = []
        for row in m:
            pose = pose + row.split('\t')
    else:
        raise NotImplementedError('Only opencv and ifindviewer file format implemented')

    pose = np.array([float(x) for x in pose if x!='']) 
    return pose.reshape(4,4)


def write_pose(pose, fname, fformat='opencv'):
    """
    Write given pose to the given file
    Arguments
    ---------
    pose: numpy 4X4 array
        Input pose to be written to a file
    fname: str
        file where the pose is to be written
    fformat: str
        format in which the pose is written. Supports 'opencv' only.
    Returns
    -------
    None

"""
    assert fformat=='opencv', 'Supports only opencv format'
    header = """%YAML:1.0
---
pose: !!opencv-matrix
   rows: 4
   cols: 4
   dt: d
   data:"""
    if isinstance(pose, np.ndarray):
        pose = pose.ravel().tolist()
    fl = f'{header} {pose}'
    #print(fl)
    with open(fname, 'w') as fil:
        fil.write(fl)


def relative_poses(poses, ref=None):
    """
    Return relative transformations of input poses w.r.t reference pose
    Arguments
    ---------
    poses: list of numpy 4X4 arrays
        Input pose(s) for which the relative poses are to be computed
    ref: [optional] numpy 4X4 array
        reference pose            
    Returns
    -------
    rel_poses: list of numpy 4X4 arrays
        Relative transformations (poses) of input poses w.r.t. ref pose
        If None, takes the first pose in the list as the reference pose.

"""
    if ref is None:
        ref = poses[0]

    ref_inv = np.linalg.inv(ref)
    rel_poses = []
    for pose in poses:
        rel_poses.append(np.matmul(ref_inv, pose))

    return rel_poses
