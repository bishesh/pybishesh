# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
My shell functions.
"""

import subprocess
import shutil as su
import os
import os.path as op

def run(cmd, print_cmd=False):
    """
    Run the input command in shell.
    Arguments
    ---------
    cmd : string
        command to be executed
    print_cmd : boolean
        print or not the executed command    
    """
    if print_cmd:
        print(cmd + '\n') 
    subprocess.call(cmd, shell=True)


def make_dirs(dirs):
    """
    Create directories if it does not exist already.
    Arguments
    ---------
    dirs : string or list/tuple of strings
        directory or a list of directories        
    
    """
    if not isinstance(dirs, (tuple,list)):
        dirs = [dirs]
    
    for curr_dir in dirs:
        if not os.path.exists(curr_dir):
            os.makedirs(curr_dir)
    return


def prefix_to_fname(prefix, fname):
    """
    Add prefix to the input filename and return the new filename.
    You can give filename with full path, I will extract only the
    filename, add prefix and then return with full path.
    
    Arguments
    ---------
    prefix : string
        prefix to be added to the filename
    fname : string
        filename (just the filename) or with full path.

    Returns
    -------
    string
    filename with the added prefix
    """
    return op.join(op.dirname(fname),
                   prefix + op.basename(fname))


def copy_file(src, dst, via_ssh=False):
    """
    Copy src file into dst directory or as dst file.
    Arguments
    ---------
    src : string
        source filename to be copied
    dst : string
        destination full path with the filname, or destination directory.
        If it is a directory, the copied file will have the same name as the source.
    via_ssh : boolean
        If True, uses 'scp src dst', else uses 'shutil.copy(src, dst)'
    """
    if via_ssh:
        cmd = 'scp %s %s' % (src, dst)
        run(cmd)
    else:
        su.copy(src, dst)
    return


def copy_dir_tree(src, dest):
    """
    Copy entire directory tree rooted at the src directory.
    Uses shutil interface.
    """
    try:
        su.copytree(src, dest)
    # Directories are the same
    except su.Error as ex:
        print('Directory not copied. Error: %s' % ex)
    # Any error saying that the directory doesn't exist
    except OSError as ex:
        print('Directory not copied. Error: %s' % ex)


def get_files(in_dir):
    """
    Returns a list of all the files (not subdirectories) in in_dir
    """
    from os import listdir
    from os.path import isfile, join
    only_files = [f for f in listdir(in_dir) if isfile(join(in_dir, f))]
    return only_files
