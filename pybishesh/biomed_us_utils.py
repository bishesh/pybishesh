# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
Utility functions related to biomedical ultrasound images.
Mostly uses SimpleITK.
"""
import SimpleITK as sitk
import numpy as np


def sector_mask(in_img, thres_vals=(0, 0.9, 1), dilate_erode_rad=2):
    """
    Create a sector mask by creating a binary mask with zeros outside
    image frustum.
    The input image is thresholded with the given range.
    The holes in the image is removed using morphological operations of
    dilation followed by erosion.
    Arguments
    ---------
    in_img : sitk image or its filename
    thres_vals : 3-tuple of floats
        (lower_val, upper_val, mask_val)
    dilate_erode_rad : int
        Radius to use for dilation and erosion.

    Returns
    -------
    sector : sitk image
        binary mask with 1 in sector and 0 in background.

    """
    from .biomed_imutils import img_or_array

    in_img = img_or_array(in_img, 'image')
    # Create a sector mask
    thres_img = sitk.Threshold(in_img, *thres_vals)
    dilator = sitk.BinaryDilateImageFilter()
    dilator.SetKernelRadius(dilate_erode_rad)
    dilated_img = dilator.Execute(thres_img)

    eroder = sitk.BinaryErodeImageFilter()
    eroder.SetKernelRadius(dilate_erode_rad)
    sector = eroder.Execute(dilated_img)
    return sector


def sector_intersection(mask, scale = 255, show_images=False,
                        slice_idx=None, slice_dir=None):
    """
    Computes an intersection and angle between two sector lines of ultrasound sector mask.
    If the input is a 3D volume, must provide slice direction and slice index to
    extract the 2D slice.
    Detects Hough lines, and uses the two highest voted lines to get an intersection
    point.
    Depends on scikit-image to compute Hough lines.

    Arguments
    ---------
    mask : numpy ndarray or sitk image of uint8 or str
        binary mask image (or its filename) with sector having value 255 and background 0.
        The values and the type of image is important. See the following link to
        understand why this is the case:
        http://scikit-image.org/docs/stable/user_guide/data_types.html
    scale : int
        Factor to multiply input mask before processing.
        Useful if the input mask is a binary image with values 0 and 1.
        Scale of 255 will make it the image of 0 and 255 as required.
    show_images : Boolean
        Show images with lines overlayed on the input image (or selected slice)
    slice_idx : int
        Slice position to extract in the input mask is a 3D volume.
    slice_dir : int
        0, 1 or 2 The direction axis to extract the slice.

    Returns
    -------
    pt : 2-tuple and float
        intersection point and angle between the lines in degrees.
    """
    from .numpy_utils import slice_simple
    from .biomed_imutils import img_or_array
    from math import isclose, sin, cos
    from skimage import feature
    from skimage.transform import hough_line, hough_line_peaks
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    mask = img_or_array(mask, 'array')
    if slice_idx:
        mask = slice_simple(mask, slice_idx, slice_dir)

    #print(f'img min : {mask.min()}, max : {mask.max()}, dtype : {mask.dtype}')
    edge = feature.canny(mask*scale)
    h, theta, d = hough_line(edge)
    accum, angles, dists = hough_line_peaks(h, theta, d, num_peaks=2)

    if len(accum) == 2:
        thetas = np.array(angles)
        rhos = np.array(dists)
        A = np.array([
            [sin(thetas[1]), -sin(thetas[0])],
            [-cos(thetas[1]), cos(thetas[0])]
            ])
        if isclose(thetas[1], thetas[0]):
            ang = 0
            print(f'two lines are parallel')
        else:
            ang = thetas[1]-thetas[0]
            meet_pt = np.matmul(A, rhos) / sin(ang)

    if show_images:
        # Generating figure 1.
        fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(12, 6))
        plt.tight_layout()

        ax0.imshow(mask, cmap=cm.gray)
        ax0.set_title('Input slice')
        ax0.set_axis_off()

        ax1.imshow(np.log(1 + h), extent=[np.rad2deg(theta[-1]), np.rad2deg(theta[0]),
                   d[-1], d[0]], cmap=cm.gray, aspect=1/1.5)
        ax1.set_title('Hough transform')
        ax1.set_xlabel('Angles (degrees)')
        ax1.set_ylabel('Distance (pixels)')
        ax1.axis('image')

        ax2.imshow(edge, cmap=cm.gray)
        row1, col1 = edge.shape
        for _, angle, dist in zip(accum, angles, dists):
            y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
            y1 = (dist - col1 * np.cos(angle)) / np.sin(angle)
            ax2.plot((0, col1), (y0, y1), '-r')
        ax2.axis((0, col1, row1, 0))
        ax2.set_title('Edges (white) and detected lines (red)')
        ax2.set_axis_off()

        plt.show()

    return meet_pt, np.rad2deg(ang)


def remove_zeros_inside_sector(in_img, replace_val=1,
                               non_existent_val=-1, dilate_erode_rad=2):
    """
    Replace all zeros present inside sector while preserving all the zero values outside the sector.
    Uses `sector_mask` to identify the US sector (frustum).
    Arguments
    ---------
    in_img : SimpleITK image
        Input image
    replace_val : same type as voxel type of in_img
        Value to replace all zeros inside the sector.
    non_existent_val : int
        Any value not present in in_img
    dilate_erode_rad : int
        Dilation-erosion kernel radius used in creating sector mask.
    Returns
    -------
    out_img : SimpleITK image
        Output image with holes inside the sector filled with 1.
    """

    in_img_arr = sitk.GetArrayFromImage(in_img)
    orig_type = in_img_arr.dtype
    img_arr = in_img_arr.astype('int')
    mask_img = sector_mask(in_img, dilate_erode_rad=dilate_erode_rad)
    mask_arr = sitk.GetArrayFromImage(mask_img)
    img_arr[mask_arr==0] = non_existent_val
    img_arr[img_arr==0] = replace_val
    img_arr[img_arr==non_existent_val] = 0
    img_arr = img_arr.astype(orig_type)
    out_img = sitk.GetImageFromArray(img_arr)
    out_img.CopyInformation(in_img)
    return out_img
