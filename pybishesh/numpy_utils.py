# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
Utility functions for numpy.
"""

import numpy as np


def slice_simple(arr, indxs, axis):
    """
    Return a fast VIEW of input arr, with chosen indices along a chosen axis.
    This can be achieved by np.take() as well, but np.take() returns a COPY of the array
    because it has much more functionality supporting advanced indexing.
    
    Taken from: EellkeSpaak's answer at:
    https://stackoverflow.com/questions/24398708/slicing-a-numpy-array-along-a-dynamically-specified-axis

    Arguments
    ---------
    arr : ndarray
        input numpy array
    indxs : sequence
        Indices to slice
    axis : int
        Axis along which to extract the indices.
    
    Returns
    -------
    Desired View from the input array

    """
    sl = [slice(None)] * arr.ndim
    sl[axis] = indxs
    return arr[sl]

def is_identity(mat):
    """
    Check if the input 2D array is an identity matrix or not
    Arguments
    ---------
    mat: 2d numpy array
        input matrix
    Returns
    -------
    bool
"""
    return (mat.shape[0]==mat.shape[1] and
            np.allclose(mat, np.eye(mat.shape[0])))
