# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
Utility functions related to file reading and handling.
"""

def lines_as_list(fname, delim):
    """
    Return a list that contains all the lines present in the file fname
    except those line that starts with delim.
    """
    with open(fname, 'r') as fil:
        return [line.strip() for line in fil if not line.startswith(delim)]


def dict_from_file(fname):
    """
    Read a python dictionary stored in a file. Useful for having config files options database that
    is directly taken as dict into the code.
    Idea taken from:
    http://stackoverflow.com/questions/11026959/python-writing-dict-to-txt-file-and-reading-dict-from-txt-file
    """
    import ast
    with open(fname, 'r') as in_fl:
        fl_as_string = in_fl.read()
        dict_from_fl = ast.literal_eval(fl_as_string)
    return dict_from_fl


def rem_num_of_lines(in_file, start_string):
    """
    Returns the number of lines that is remaining in the given file starting
    from the first appearance of the given start string.
    ---------
    Arguments
    ---------        
    in_file: string
        Input file name
    start_string: string
        Start string to be searched in the input file
    
    Returns
    -------
    lines_rem: int
        Number of lines remaining in <in_file> after the first appearance of <start_string>.
        Includes the line where <start_string> first appeared in the count.
    """
    lines_rem = 0
    start_string_found = False
    with open(in_file) as in_file:        
        for line in in_file:
            if not start_string_found:
                if start_string in line:
                    start_string_found = True
            if start_string_found is True:
                lines_rem += 1

    return lines_rem
