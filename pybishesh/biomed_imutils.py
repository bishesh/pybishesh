# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>

"""
Utility functions related to biomedical images.
Mostly uses SimpleITK.
"""
from .myshell import run
from .numpy_utils import slice_simple
import numpy as np

import SimpleITK as sitk

def img_or_array(img, return_type='array'):
    """
    Return either sitk image or numpy array from the input filename or
    sitk image.
    Arguments
    ---------
    img : str or sitk image or numpy array
        input image filename or sitk image
    return_type : str
        'array' or 'image'. Returns numpy array if 'array' else return sitk image.
    """
    assert(isinstance(img, (str, sitk.SimpleITK.Image, np.ndarray)))
    if isinstance(img, np.ndarray) and (return_type == 'image'):
        raise Exception('Image return type valid only when input is string or sitk image')

    if isinstance(img, str):
        img = sitk.ReadImage(img)

    if isinstance(img, sitk.SimpleITK.Image) and (return_type == 'array'):
        img = sitk.GetArrayFromImage(img)

    return img

def itk_snap(itksnap_bin, img_files, seg_files=None):
    """
    Display image (optionally along with a paired segmentation map) one at a time.
    If multiple images are given, asks to continue or not before displaying next image.
    
    Arguments
    ---------
    itksnap_bin: string
        ITK-SNAP Binary file with full path
    img_files: string or list of strings
        Image file(s) to be displayed
    seg_files: [optional] string or a list of strings
        Segmentation file(s) to be overlayed on img_files
    """
    if not isinstance(img_files, (list, tuple)):
        img_files, seg_files = [img_files], [seg_files]
    elif seg_files is None:
        seg_files = [seg_files]*len(img_files)
    nimgs = len(img_files)
    for img_file, seg_file in zip(img_files, seg_files):
        cmd = f'{itksnap_bin} -g {img_file}'
        if seg_file:
            cmd += f' -s {seg_file}'
        run(cmd)
        if nimgs > 1:
            quit = input('Enter to continue, n+Enter to quit ?')
            if quit.upper() == 'N':
                break
    return

def slice_pil(img, slc_dir, slc_idx=None,
              seg=None, seg_mask_away=0, seg_alpha=0.3, seg_rgb = (0.8, 0, 0),
              out_size=None, set_width = True,
              out_file=None, quality=85):
    """
    Return (and optionally save) selected slice of the input image (and of segmentation if given.)
    as PIL image.
    If seg is given, overlays over the input image.

    Arguments
    ---------
    img : sitk image or string
        input sitk image or its filename
    slc_dir : int
        input slice direction axis. E.g. 0, 1 or 2
    slc_idx : int
        slice position (index). If None, chooses central slice in the given slice direction.
    seg : sitk image or string
        segmentation image or its filename
    seg_mask_away : int
        Label in seg image that is to be suppressed during overlay. Default is 0.
    seg_rgb : tuple of float
        (r, g, b) values in range [0, 1] to determine the color of seg label. Default (0.8, 0, 0).
    seg_alpha : float
        In range [0, 1] the alpha value to blend the segmentation overlay.
    out_size : int or tuple
        Output image size. Default None, takes the size from the chosen input image slice.
        If int given, sets width/height according to set_width parameter and computes the other
        by preserving aspect ratio.
        If tuple given, resize the input image to this size, doesn't check aspect ratio.
    set_width : boolean
        True if out_size gives width value when only one value given. Default : True
    out_file : string
        output image filename in formats such as .png, .jpg.
    quality : int
        Quality of output image to be saved. This is passed to PIL image save function

    Returns
    -------
    out_img : PIL output image
    """
    from skimage import color, img_as_float
    from PIL import Image, ImageOps
    import math

    img = img_or_array(img, 'array')
    if seg is not None:
        overlay = True
        seg = img_or_array(seg, 'array')
        assert(img.shape == seg.shape)
    else:
        overlay = False

    if slc_idx is None:
        slc_idx = img.shape[slc_dir]//2

    img_slice = img_as_float(slice_simple(img, slc_idx, slc_dir))
    # Construct RGB version of grey-level image
    img_color = np.dstack((img_slice, img_slice, img_slice))
    if overlay:
        seg_slice = img_as_float(slice_simple(seg, slc_idx, slc_dir))
        seg_mask = np.ma.masked_where(seg_slice == 0, seg_slice)

        # Construct RGB version of grey-level images
        color_mask = np.dstack((
            seg_slice*seg_rgb[0], seg_slice*seg_rgb[1], seg_slice*seg_rgb[2]))

        # Convert the input image and color mask to Hue Saturation Value (HSV) color space
        img_hsv = color.rgb2hsv(img_color)
        color_mask_hsv = color.rgb2hsv(color_mask)
        # Replace H,S of input image with that of the color mask
        img_hsv[..., 0] = color_mask_hsv[..., 0]
        img_hsv[..., 1] = color_mask_hsv[..., 1] * seg_alpha

        out_img = color.hsv2rgb(img_hsv)
    else:
        out_img = img_color

    out_img = Image.fromarray(np.uint8(out_img*255), mode='RGB')
    if out_size is not None:
        if isinstance(out_size, (tuple, list)):
            new_size = out_size
        else:
            if set_width:
                wh = 0
            else:
                wh = 1

            fac = out_size/out_img.size[wh]
            new_size = tuple([math.floor(fac*x) for x in out_img.size])

        out_img = out_img.resize((new_size), Image.ANTIALIAS)
        # TODO: Add feature to crop with selected new size.
        #out_img = ImageOps.fit(out_img, new_size, Image.ANTIALIAS)

    if out_file:
        out_img.save(out_file, quality=quality)

    return out_img


def nib_to_sitk(nib_affine, nib_data, change_orig=True):
    """
    Convert nibabel image to sitk image.
    This is not a fully robust solution yet. The direction info. in nibabel affine matrix and itk
    direction, origin seems confusing. Argument change_orig allows changing the sign of either
    origin or direction in sitk image.

    Arguments
    ---------
    nib_affine : numpy 4x4 array
        Affine matrix of the nibabel image
    nib_data : numpy array
        nibabel image data in numpy array form
    change_orig : boolean
        If True, the sign of diagonal elements in nib_affine used to change signs in sitk origin.
        If False, the signs will be reflected in sitk direction matrix.

    Returns
    -------
    SimpleITK image

    """    
    import SimpleITK as sitk
    import numpy as np

    affn = nib_affine.copy() # We will be changing references of affn, so this is to avoid changing
    # object referenced by the mutable nib_affine ndarray!
    
    affn_diag = affn.diagonal()[0:3]
    spacing = np.abs(affn_diag)
    orig = affn[0:3, 3]
    direction = affn[0:3, 0:3]
    if change_orig:
        orig = orig * (spacing / affn_diag)
        np.fill_diagonal(direction, 1)
    else:
        np.fill_diagonal(direction, (spacing / affn_diag))
    
    img = sitk.GetImageFromArray(np.swapaxes(nib_data, 0, 2))
    img.SetOrigin(orig)
    img.SetSpacing(spacing)
    img.SetDirection(np.ravel(direction))

    return img    


def img_info(img):
    """
    Return a dict with basic image info coming from sitk image
    Arguments
    ---------
    img : img file or SimpleITK image
    """
    if isinstance(img, str):
        img = sitk.ReadImage(img)

    return {'size' : img.GetSize(),
            'origin': img.GetOrigin(),
            'spacing': img.GetSpacing()}


def imgs_size_stat(imgs, dim=3):
    """
    Return dictionary with stats of the sizes of input images
    Arguments
    ---------
    imgs : list or tuple of sitk images or their filenames
        images or their filenames whose size stats are to be returned
    dim : int
        image dimension default 3
    Returns
    -------
    size_stat : dict
        dictionary with following keys:
        'mean', 'median', 'min', 'max'
    """
    import numpy as np
    
    num_imgs = len(imgs)    
    sizes = np.zeros((num_imgs, dim), dtype='int')
    for idx, img in enumerate(imgs):
        if isinstance(img, str):
            img = sitk.ReadImage(img)
        
        sizes[idx,:] = img.GetSize()

    dct = {}
    dct['min'] = sizes.min(axis=0)
    dct['max'] = sizes.max(axis=0)
    dct['mean'] = sizes.mean(axis=0)
    dct['median'] = np.median(sizes, axis=0)

    return dct
    
