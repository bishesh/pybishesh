# emacs: -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-

# Author: Bishesh Khanal <bisheshkh@gmail.com>
# 	  King's College London, UK
#	  Department of Computing, Imperial College London, UK

"""
Module for virtual pinhole camera model.
"""

def project(f, c, p):
    """
    Arguments
    f : tuple or list
        focal lengths fx and fy
    c : tuple or list
        principle point components cx and cy
    p : tuple or list
        3D point x, y, z to project to an image plane
        
    Returns
    pixel_pos : tuple
        (u, v) position: a projection of p on the image plane 
    """
    return (p[0]*f[0]/p[3] + c[0], p[1]*f[1]/p[3] + c[1])

def reproject(f, c, uv, d):
    """
    Arguments
    f : tuple or list
        focal lengths fx and fy
    c : tuple or list
        principle point components cx and cy
    uv : tuple or list
        (u, v) position on image plane for which a 3D point is to be backprojected
    d  : float
        depth value at (u, v)
    Returns
    p : tuple
        (x, y, z) 3D point which got projected to (u, v)
    """
    return (d*(uv[0]-c[0])/f[0],
            d*(uv[1]-c[1])/f[1],
            d)

def get_va(width, d):
    """
    Get viewing angle of a camera with a given distance to cover the desired width
    given in world coordinate.    
    Arguments
    ---------
    width : float
        width of the scene to be covered in world coordinates
    d : float
        camera distance to the plane at which the input width is desired, in world coordinates.
    
    Returns
    -------
    va : float
        viewing angle in degrees
    """
    import math
    return math.degrees(2*math.atan(width/2./d))

def get_f(u, c, Z, X):
    """
    Get a focal length when, for a given position in an image its corresponding point
    in the real world is known.
    Arguments
    ---------
    u : float
        pixel position along u (or v)
    c : float
        principle point of the image
    Z : float
        z co-ordinate of the corresponding point in physical unit (e.g. mm)
    X : float
        X x (or y) coordinate of the corresponding point in the same unit as Z
    
    Returns
    -------
    f : float
        focal length in pixel units
    """
    return ( (u-c) * Z / X)


def get_Z(f, u, c, X):
    """
    Get Z value (depth) in real unit of a point.
    Arguments
    ---------
    f : float
        Camera focal length (in pixel co-ordinates)
    u : float
        Pixel co-ordinate of the x (or y) axis
    c : float
        principle point in pixel co-ordinate along the same axis as for u.
    X : float
        x (or y) axis coordinate value of the 3D point in real units (e.g. mm)

    Returns
    -------
    Z : float
        Z value in real units, i.e. real distance (along principal axis ) of the point from
        the camera.
    """
    return (f*X/(u-c))


def dbuffer_to_z(d, zn, zf, sf):
    """
    Arguments
    ---------
    d : float
        depth buffer value in range [0, 1]
    zn : float
        near z-plane of camera frustum
    zf : float
        far z-plane of camera frustum
    sf : float
        scale factor; scales output z by this factor
    Returns
    -------
    z : float
        depth in the same unit as zn and zf but scaled by sf
    """
    return sf*(
        -zf*zn/((zf-zn)*(d - zf/(zf-zn)))
    )
