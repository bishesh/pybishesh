#!/usr/bin/python
""" 
Helper script to manage the database of resources of all the files in a given directory.
Useful to manage citation source of images and videos that you use in your presentation or reports.
The script creates a dictionary whose keys are the filenames in the given directory.
The values for each key is asked to the user who can add the source information for each of these
files/keys.
The dictionary is stored as a json file.
"""
# Author: Bishesh Khanal <bisheshkh@gmail.com>
#
#

import argparse as ag
import os
import sys
import json

from pybishesh.myshell import get_files


def get_input_options():
    """ Command line interface, get input options and interact with the user.
    """
    parser = ag.ArgumentParser('Write filenames of the files in input directory'
                               ' to the given file.')
    parser.add_argument('out_file', help='output json file where the filenames are to '
                        'be written/modified')
    parser.add_argument('in_dir', help='input directory to look for source files.')
    parser.add_argument('--modify', help='modify existing entries of these files')
    parser.add_argument('--modify_all', help='modify all existing entries')
    ops = parser.parse_args()
    return ops

def main():
    """ Add filenames of the files in a directory to a file if not already
    present"""
    ops = get_input_options()
        
    if os.path.isfile(ops.out_file):
        with open(ops.out_file, 'r') as f:
            curr_dict = json.load(f)
    else: # Create empty dictionary if input file is a new one
        curr_dict = {}

    all_fnames = get_files(ops.in_dir)
    new_fnames = [fname for fname in all_fnames if fname not in curr_dict.keys()]
    tmp_dict = {}
    
    for fname in new_fnames:
        cite_src = input('Enter source of %s: ' % fname)
        online_url = input('Enter online url (e.g. google drive id) of %s: ' % fname)
        tmp_dict[fname] = [cite_src, online_url]

    if ops.modify is not None:
        print(f'old entry for {ops.modify}: {curr_dict[ops.modify]}')
        cite_src = input('Enter source of %s: ' % ops.modify)
        online_url = input('Enter online url (e.g. google drive id) of %s: ' % ops.modify)
        tmp_dict[ops.modify] = [cite_src, online_url]
        
    if not tmp_dict:
        print('no new entries to add')
    else:
        print(f'added entries: {tmp_dict}')        
        curr_dict.update(tmp_dict)
        with open(ops.out_file, 'w') as f:
            json.dump(curr_dict, f, indent=4)

if __name__ == "__main__":
    main()
