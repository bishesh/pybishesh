# pybishesh #

This repository contains utility modules and functions that I have been using for a range of tasks such as:

* file handling, shell/os command like tools callable from python programs
* numpy utilities
* transformation matrices, poses utilities
* biomedical image handling, ultrasound image processing
* camera pinhole model

## Dependencies

* python3.6+

## Installation

`pip install git+https://<username>@gitlab.com/bishesh/pybishesh.git`
