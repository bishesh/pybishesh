#!/usr/bin/env python
"""pybishesh: A wide range of python utilities.

This repository contains utility modules and functions that I have been using for a range of tasks
such as:
+ file handling, shell/os command like tools callable from python programs
+ numpy utilities
+ transformation matrices, poses utils
+ biomedical image handling, ultrasound image processing
+ camera pinhole model

## Dependencies

* python3.6+

"""

import os
from setuptools import setup, find_packages
from codecs import open

with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'requirements.txt')) as f:
    requirements = f.read().splitlines()

with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'license.txt')) as f:
    license = f.read()

setup(
    name="pybishesh",
    version="0.1.0",
    description="Wide range of python based tools used mostly but not limited to biomedical image processing",
    url="https://gitlab.com/bishesh/pybishesh",
    author="Bishesh Khanal",
    author_email="bisheshkh@gmail.com",
    # Requirements
    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=requirements,
    setup_requires=requirements,
    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=["build", "examples", "doc"]),
    # Package where to put the extensions. Has to be a prefix of build.py.
    ext_package="",
    # What does your project relate to?
    keywords='Python utilities biomedical image processing computer vision',
    long_description=long_description,
    license=license,
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
